import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ProjectModule } from '../../projects/project.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [ProjectModule, BrowserModule, AppRoutingModule, ProjectModule],
  providers: [],
  exports: [ProjectModule],
  bootstrap: [AppComponent],
})
export class AppModule {}

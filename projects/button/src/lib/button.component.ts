import { Component } from '@angular/core';

@Component({
  selector: 'wd-button',
  standalone: false,
  template: ` <p>button works!</p> `,
  styles: ``,
})
export class ButtonComponent {}

import { Component } from '@angular/core';

@Component({
  selector: 'wd-modal',
  standalone: false,
  template: ` <p>modal works!</p> `,
  styles: ``,
})
export class ModalComponent {}

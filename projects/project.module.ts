import { NgModule } from '@angular/core';
import { ButtonComponent } from './button/src/public-api';
import { ModalComponent } from './modal/src/public-api';
import { TabComponent } from './tab/src/public-api';
import { TabsComponent } from './tabs/src/public-api';

@NgModule({
  declarations: [ButtonComponent, ModalComponent, TabComponent, TabsComponent],
  imports: [],
  exports: [ButtonComponent, ModalComponent, TabComponent, TabsComponent],
})
export class ProjectModule {}
